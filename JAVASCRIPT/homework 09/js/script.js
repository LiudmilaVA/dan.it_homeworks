function openHero(event, heroName) {
    const tabContent = document.querySelectorAll('.tab-content');
    const tabsTitle = document.querySelectorAll('.tabs-title');


    tabContent.forEach((content) => {
        content.style.display = 'none';
    });

    tabsTitle.forEach((title) => {
        // title.className = title.className.replace('active', '');
        title.classList.remove('active');
    });

    document.getElementById(heroName).style.display = 'block';
    event.currentTarget.className += " active";
}

document.getElementById('defaultOpen').click();
