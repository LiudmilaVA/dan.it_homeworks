let firstName = prompt("Please, enter your name: ");
let lastName = prompt("Please, enter your lastname: ");
let birthday = prompt('Please, enter your birthday: ', 'dd.mm.yyyy');

function createNewUser(firstName, lastName, birthday) {
    const newUser = {
        firstName,
        lastName,
        birthday,
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getAge() {
            let birthdayArray = this.birthday.split('.');
            [ birthdayArray[0], birthdayArray[1] ] = [ birthdayArray[1], birthdayArray[0] ];

            return parseInt((new Date().getTime() - new Date(birthdayArray)) / (24 * 3600 * 365.25 * 1000))
            // 24 hours(in the day) * 3600 s (=60 min) * 365,25 days (in year) * 1000 years (in millennia)

            // let birthdayArray = this.birthday.split('.');
            // const BIRTHDAY = {
            //     'day': Number(birthdayArray[0]),
            //     'month': Number(birthdayArray[1]),
            //     'year': Number(birthdayArray[2])
            // };
            //
            // let today = new Date();
            // let todayMonth = Number(today.getMonth()) + 1;
            // let todayDay = Number(today.getDate());
            // let todayYear = Number(today.getFullYear());
            //
            // let userAge = todayYear - Number(BIRTHDAY.year);
            // if (todayMonth < BIRTHDAY.month || todayMonth === BIRTHDAY.month && todayDay < BIRTHDAY.day) {
            //     userAge--;
            // }
            // return userAge;
        },
        getPassword() {
            return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6));
        }
    };

    return newUser;
}

const newUser = createNewUser(firstName, lastName, birthday);

console.log(newUser);
console.log(`User's login: ${newUser.firstName} ${newUser.lastName} -> ${newUser.getLogin()}`);
console.log(`User's age: ${newUser.getAge()}`);
console.log(`User's password : ${newUser.getPassword()}`);



