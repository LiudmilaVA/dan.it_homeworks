const allButtons = document.querySelectorAll('.btn');

window.addEventListener('keydown', (event) => {
    allButtons.forEach((blueBtn) => {
        blueBtn.classList.remove('active');
    });

    allButtons.forEach((btn) => {
        if(event.code === 'Key' + btn.textContent || event.key === btn.textContent) {
            btn.classList.add('active');
        }
    });
});