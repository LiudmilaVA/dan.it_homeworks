function isObject(object) {
    let type = typeof object;
    return type === 'object' && type !== null;
}

function isArray(array) {
    return Array.isArray(array);
}

function getDeepClone(object) {
    let clone = {};

    for(let key in object) {
        if (isArray(object[key])) {
            clone[key] = [];
            for (let item of object[key]) {
                clone[key].push(item);
            }
        } else if (isObject(object[key])) {
            clone[key] = getDeepClone(object[key]);
        } else {
            clone[key] = object[key];
        }
    }
    return clone;
}

function getBestDeepClone(object) {
    return Object.assign({}, object);
}

const USER = {
    age: 20,
    name: {
        first: 'Jon',
        second: 'Dou',
        parents: {
            father: 'Liam',
            mother: 'Hidi',

        },
        relatives: ['Dan', 'Jen', 'Barbara']
    }
};
const CLONE = getDeepClone(USER);

console.log("----------------------DEEP CLONE---------------------");
console.log(`Change mother for user: ${USER.name.parents.mother = 'Catrice'}`);
console.log(`Still not change mother for clone: ${CLONE.name.parents.mother}`);


