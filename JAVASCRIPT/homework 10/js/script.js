const enterPassword = document.querySelector('[name = enter-password]');
const reaffirmPassword = document.querySelector('[name = reaffirm-password]');
const passwordForm = document.querySelector('.password-form');
const confirmBtn = document.querySelector('button[type=submit]');

/*------------------СЛУШАТЕЛЬ СОБЫТИЙ-----------------------*/
passwordForm.addEventListener('click', (event) => {
    if (event.target.matches('i')) {
        changeIcon(event.target);
    } else if (event.target === confirmBtn) {
        sendPassword(event);
    }
});

/*------------------------ФУНКЦИЯ ИЗМЕНЕНИЯ ИКОНКИ------------------*/
function changeIcon(icon) {
    const current = icon.parentElement.querySelector('input');
    // const current = icon.previousElementSibling;

    if (current.type === 'text') {
        current.type = 'password';
        icon.className = icon.className.replace('fa-eye', 'fa-eye-slash');
    } else {
        current.type = 'text';
        icon.className = icon.className.replace('fa-eye-slash', 'fa-eye');
    }
};

/*--------------------ФУНКЦИЯ ОТПРАВКИ ФОРМЫ-------------------------*/
function sendPassword(event) {
    const errorMessage = document.querySelector('.error-message') || document.createElement('p');
    event.preventDefault();

    if (enterPassword.value === reaffirmPassword.value) {
        errorMessage.remove();
        alert('You are welcome!');
    } else {
        errorMessage.classList.add('error-message');
        errorMessage.textContent = 'Нужно ввести одинаковые значения!';
        errorMessage.style.color = 'red';

        confirmBtn.insertAdjacentElement('beforebegin', errorMessage);
    }
}
