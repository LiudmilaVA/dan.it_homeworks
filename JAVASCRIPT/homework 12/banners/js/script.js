const imagesWrapper = document.querySelector('.images-wrapper');
const imageList = imagesWrapper.querySelectorAll('.image-to-show');
const stopButton = document.querySelector('.stop-button');
const continueButton = document.querySelector('.continue-button');

let imageIndex = 0;

showImages();

/*--------------ТАЙМЕР ПОВТОРЕНИЙ-----------------*/
let timer = setInterval(showImages, 1000);

/*-------------------СЛАЙД-ШОУ-----------------------*/
function showImages() {
    imageList.forEach((image) => {
        image.classList.add('inactive');
    });

    imageIndex++;
    if (imageIndex > imageList.length) {
        imageIndex = 1;
    }
    imageList[imageIndex - 1].classList.remove('inactive');
}

/*-----------------------СТОП-КНОПКА-----------------*/
stopButton.addEventListener('click', () => {
        timer = clearInterval(timer);
        stopButton.setAttribute('disabled', '');
        continueButton.removeAttribute('disabled');
});

/*--------------------ВОЗОБНОВИТЬ-КНОПКА---------------*/
continueButton.addEventListener('click', () => {
        timer = setInterval(showImages, 1000);
        continueButton.setAttribute('disabled', '');
        stopButton.removeAttribute('disabled');
});
