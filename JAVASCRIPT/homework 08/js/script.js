const wrapper = document.querySelector('.wrapper-input');
const input = wrapper.querySelector('.price-input');
const wrapperPrice = document.querySelector('.wrapper-price');
const errorMessage = document.querySelector('.error-text');
const currentPrice = wrapperPrice.querySelector('.price-current');
const buttonClose = wrapperPrice.querySelector('.price-btn-close');

input.addEventListener('focusout', () => {
   if (input.value < 0 || input.value == '') {
       if(currentPrice) {
           currentPrice.classList.remove('active');
           buttonClose.classList.remove('active');
       }
       addErrorMessage();
   } else {
       if (errorMessage.classList.contains('active')) {
           errorMessage.classList.remove('active');
       }
       addCurrentPrice();
   }
});

function addErrorMessage() {
    input.style.outlineColor = 'red';
    errorMessage.classList.add('active');
}

function addCurrentPrice() {
    currentPrice.textContent = `Текущая цена: $${input.value}`;
    currentPrice.classList.add('active');
    buttonClose.classList.add('active');
    input.classList.add('active');
    input.style.outlineColor = '#117243';

    buttonClose.addEventListener('click', () => {
        currentPrice.classList.remove('active');
        buttonClose.classList.remove('active');
        input.value = null;
    });
}