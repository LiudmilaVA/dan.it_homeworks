let firstNumber = Number(prompt("Please, enter the first number: "));
if (isNaN(firstNumber) || firstNumber == '') {
    firstNumber = Number(prompt("Please, enter the first number: ", "1"));
}

let secondNumber = Number(prompt("Please, enter the second number: "));
if (isNaN(secondNumber) || secondNumber == '') {
    secondNumber = Number(prompt("Please, enter the second number: ", "6"));
}

let operation = prompt("Please, enter one of the operation (+ - * /): ");

function calcNumbers(number1, number2, symbol) {

    switch (symbol) {
        case '+':
            return number1 + number2;
        case '-':
            return number1 - number2;
        case  '*':
            return number1 * number2;
        case '/':
            return number1 / number2;
        default:
            return alert("Error!");
    }
}
console.log(`${firstNumber + ' ' + operation + ' ' +secondNumber} = ${calcNumbers(firstNumber, secondNumber, operation)}`);
