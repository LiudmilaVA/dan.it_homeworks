const themeButton = document.querySelector('.header--theme-btn');

const bodyElem = document.body;
const headerBgImage = document.querySelector('.header');
const headerMenu = document.querySelector('.header-menu');
const clientsWrapper = document.querySelector('.clients-wrapper');
const imagesList = document.querySelectorAll('img[src$=jpg]');

const array = [bodyElem, headerBgImage, headerMenu, clientsWrapper];

array.forEach((object) => {
    checkLocalStorage(object);
});

imagesList.forEach((image) => {
    checkLocalStorage(image);
});

/*-------------------------ПЕРЕКЛЮЧАТЕЛЬ ТЕМЫ-------------------------------*/
themeButton.addEventListener('click',(event) => {
    array.forEach((object) => {
        object.classList.toggle('invert-theme');
        setLocalStorage(object);
    });

    imagesList.forEach((image) => {
        image.classList.toggle('invert-theme');
        setLocalStorage(image);
    });
});

/*----------------------СОХРАНИТЬ ЭЛЕМЕНТЫ В ХРАНИЛИЩЕ------------------------*/
function setLocalStorage(element) {
    if(element.classList.contains('invert-theme')) {
        localStorage.setItem(element.nodeName, 'true');
    } else {
        localStorage.setItem(element.nodeName, 'false');
    }
}

/*---------------------ВЗЯТЬ ЭЛЕМЕНТЫ ИЗ ХРАНИЛИЩА------------------------------*/
function checkLocalStorage(element) {
    if (localStorage.getItem(element.nodeName) === 'true') {
        element.classList.add('invert-theme');
    }
}












