let number = Number(prompt("Please, enter the number: "));

while (!Number.isInteger(number))  {
    number = Number(prompt("Please, enter the number: "));
}
console.log(`--------------------NUMBERS MULTIPLE OF 5 to ${number}--------------------`);

let counter = 0;
for (let i = 1; i <= number; i++) {
    if (i % 5 === 0) {
        console.log(i);
        counter++;
    }
}
if (counter === 0) {
    console.log("Sorry, no numbers");
}

