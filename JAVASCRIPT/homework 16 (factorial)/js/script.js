let number = prompt("Please, enter the positive number: ");

if (isNaN(number) || number === '' || number < 0) {
    number = prompt("Please, enter the positive number: ", "5");
}

// function calcFactorial(number) {
//     let factorial = 1;
//     for (let i = 1; i <= number; i++) {
//         factorial *= i;
//     }
//     return factorial;
// }

function calcFactorial(number) {
    if (+number === 1 || +number === 0) {
        return 1;
    } else {
        return number * calcFactorial(number - 1);
    }
}

// не совсем верно, факториал "0" будет "1", а такой вариант это не учитывает:
// function calcFatorial (number) {
//     return (number != 1) ? number * calcFatorial(number - 1) : 1;
// }

console.log(`The factorial of number !${number} = ${calcFactorial(number)}`);
