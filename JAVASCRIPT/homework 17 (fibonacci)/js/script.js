/*-------------------------LOOP FIBONNACI------------------*/
// function calcFibonnaci (F0, F1, n) {
//     let result = 0;
//     if (F0 < 0 || F1 < 0) {
//         for (let i = 3; i <= n; i++) {
//             result = F0 - F1;
//             F0 = F1;
//             F1 = result;
//         }
//     } else {
//         for (let i = 3; i <= n; i++) {
//             result = F0 + F1;
//             F0 = F1;
//             F1 = result;
//         }
//     }
//     return result;
// }

/*-------------------------RECURCION FIBONNACI------------------*/
let result = 0;

function calcFibonnaci (F0, F1, n) {
    (F0 < 0 || F1 < 0) ? result = F0 - F1 : result = F0 + F1;

    F0 = F1;
    F1 = result;

    if (n == 3) {
        return result;
    } else {
        return calcFibonnaci(F0, F1, n - 1);
    }
}
let index = prompt("Please, enter the order number: ");
alert((`The number of Fibonnaci is n[${index}] = ${calcFibonnaci(1, -1, index)}`));
