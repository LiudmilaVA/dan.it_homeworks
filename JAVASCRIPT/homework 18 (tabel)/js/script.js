const firstName = prompt("Please, enter your name: ");
const lastName = prompt("Please, enter your lastname: ");
const newUser = createNewUser(firstName, lastName);

newUser.tabel = {};

newUser.addGradesInTabel();
newUser.calcLowGrades();
newUser.calcAverageGrade();

function createNewUser(firstName, lastName) {
    const newUser = {
        firstName,
        lastName,
        addGradesInTabel() {
            const object = prompt("Enter the object: ");

            if (!object) return console.log(newUser.tabel);

            objectGrade = Number(prompt("Enter the grade: "));
            newUser.tabel[object] = objectGrade;

            return newUser.addGradesInTabel();
        },
        calcLowGrades() {
            let lowGrades = 0;

            for (let grades in newUser.tabel) {
                if (newUser.tabel[grades] < 4) lowGrades += 1;
            }
            if (!lowGrades) return console.log(`Студент ${newUser.firstName} ${newUser.lastName} переведен на следующий курс`);
        },
        calcAverageGrade() {
            let averageGrade = 0;
            let counterObjects = 0;

            for (let grades in newUser.tabel) {
                averageGrade += newUser.tabel[grades];
                counterObjects++;
            }
            if (averageGrade / counterObjects > 7) return console.log(`Студенту ${newUser.firstName} ${newUser.lastName} назначена стипендия`);
        }
    };

    return newUser;
}

