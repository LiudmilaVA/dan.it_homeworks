## Теоретический вопрос

1. Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)

- DOM-дерево - это интерфейс, который позволяет другим программам и скриптам получить
доступ к html, менять и взаимодейтсвовать с ним. DOM формируется из html-кода. html состоит из тэгов, а DOM-дерево из 
объектов. Увидеть DOM-дерево можно в панели разработчика. Объекты DOM являются узлами дерева.
- Наибольшее различие DOM-дерева в том, что он может отличатся от изначального html-кода, так как 
дерево показывает все изменения вследствие выполнения скриптов, а html же сам по себе вмещает только
написанный нами код.