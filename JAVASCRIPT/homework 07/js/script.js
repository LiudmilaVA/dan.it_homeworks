const wrapper = document.querySelector('.wrapper');

function getArray(list, listWrapper) {
    const mainList = document.createElement('ul');
    listWrapper.append(mainList);

    list.map((item) => {
        if (Array.isArray(item)) {
            getArray(item, mainList);

        } else if (typeof item === 'object') {
            getObjectArray(mainList, item);

        } else {
            mainList.insertAdjacentHTML("beforeend", `<li>${item}</li>`);
        }
   });
}
function getObjectArray(sublistWrapper, object) {
    const subList = document.createElement('ul');
    sublistWrapper.append(subList);
    for (let key in object) {
        subList.insertAdjacentHTML('beforeend', `<li>${key}: ${object[key]}</li>`);
    }
}

let timerWrapper = document.querySelector('.timer');
let seconds = 10;
let timer;

function countOffTimer() {
    timerWrapper.innerHTML = seconds;
    seconds--;

    if (seconds < 0) {
        clearTimeout(timer);
        document.body.innerHTML = '';
    } else {
        timer = setTimeout(countOffTimer, 1000);
    }
}

countOffTimer();
getArray(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'], wrapper);
getArray(['1', '2', '3', 'sea', 'user', 23], wrapper);
getArray(['JavaScript',['Not', 'a', 'number', {'this': 'is', 'a': 'new', 'object': '!'}], 'Teeeext', {'one': 1, 'two': 2, 'three': 3},  565689], wrapper);


