/*------------------------NEW USER-------------------------*/
let firstName = prompt("Please, enter your name: ");
let lastName = prompt("Please, enter your lastname: ");

function createNewUser(firstName, lastName) {
    const newUser = {
        firstName,
        lastName,
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        }
    };

    return newUser;
}

const newUser = createNewUser(firstName, lastName);

console.log('-------------------------NEW USER----------------------');
console.log(newUser);
console.log(`getLogin: ${newUser.firstName} ${newUser.lastName} -> ${newUser.getLogin()}`);


/*------------------------NEW USER WITH DESCRIPTOR-------------------------*/

function createNewUser_2(firstName, lastName) {
    const newUser_2 = {};

    Object.defineProperty(newUser_2, 'setFirstName', {
        writable: true,
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(newUser_2, 'setLastName', {
        writable: true,
        enumerable: true,
        configurable: true
    });

    return newUser_2;
}
const newUser_2 = createNewUser_2("Liudmila", "Vaskovska");

console.log('------------------NEW USER WITH DESCRIPTOR-----------------');
console.log(newUser_2);

console.log(`setFirstName: ${newUser_2.setFirstName = firstName}`);
console.log(`setLastName: ${newUser_2.setLastName = lastName}`);
console.log(newUser_2);

