debugger;
function filterBy(array, arrayType) {
    let filterArray = array.filter( (item) => {
        return typeof item !== arrayType;
    });
    return filterArray;
};
console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));