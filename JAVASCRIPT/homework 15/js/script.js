$('.news__toggle').click(function () {
    $(this).text() == 'Show less' ? $(this).text('Show more') : $(this).text('Show less');
    $('.news__item:nth-last-child(-n+4)').toggleClass('hide');
});

$(function () {
   $(window).scroll(function () {
      if($(this).scrollTop != 0) {
          $('.scroll-button').fadeIn();
      } else {
          $('.scroll-button').fadeOut();
      }
   });
   $('.scroll-button').click(function () {
      $('body, html').animate({scrollTop: 0}, 100);
   });
});