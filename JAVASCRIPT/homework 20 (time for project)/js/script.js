const speedForDay = [1, 5, 3];
const listProjects = [100, 25, 31, 15, 12];
const dateDeadline = new Date('September 30, 2019');

showDeadline(speedForDay, listProjects, dateDeadline);

function showDeadline(speed, list, date) {
    let today = new Date();
    let speedAll = 0;
    let pointsCount = 0;

    for (let worker in speed) {
        speedAll += speed[worker];
    }
    for (let task in list) {
        pointsCount += list[task];
    }

    let daysForWork = Math.ceil(pointsCount / speedAll);
    let daysOff = Math.ceil(daysForWork/5);
    console.log(`За ${daysForWork} days будет выполнен проект`);

    let daysForProject = Math.ceil((new Date(date).getTime() - today.getTime())/(24*60*60*1000));
    console.log(`${daysForProject} days дано на выполнение`);
    let daysBeforeDeadline = daysForProject - daysForWork - daysOff;
    console.log(`Количество days до дедлайна: ${daysBeforeDeadline}`);

    if (daysForProject >= daysForWork) {
        alert(`Все задачи будут успешно выполнены за ${Math.ceil(daysBeforeDeadline)} дней до наступления дедлайна!`);
    } else {
        let hoursAfterDeadline = Math.abs(daysBeforeDeadline * 24);
        alert(`Команде разработчиков придется потратить дополнительно ${Math.ceil(hoursAfterDeadline)} часов после дедлайна, чтобы выполнить все задачи в беклоге.`)
    }
}