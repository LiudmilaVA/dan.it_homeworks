class WhackAMole {
    constructor(btnStart, gameArea, cellsList, score, cellsCount, timeLevel = 1500) {
        this.btnStart = btnStart;
        this.gameArea = gameArea;
        this.cellsList = cellsList;
        this.score = score;
        this.cellsCount = cellsCount;
        this.emptyCells = [...this.cellsList];
        this.timeLevel = timeLevel;
        this.timer = null;
        this.counter = 0;
        this.cellsIndex = this._random();

    }
    start() {
        this.btnStart.textContent = 'Paused game';
        this._showing();
        this.timer = setTimeout(() => {
            this._missed();
            if (this.counter < this.cellsCount - 1) {
                this.counter++;
                this.start();
            }
        }, this.timeLevel);
    }

    stop() {
        this.btnStart.textContent = 'Start game';
        clearTimeout(this.timer);
    }

    catch(cell) {
        if (cell.classList.contains('show__cell') && !cell.classList.contains('missed__cell')) {
            this.score.textContent++;
            cell.classList.add('catching__cell');
        }
    }

    level(target) {
        if (target.classList.contains('level-easy')) {
            return this.timeLevel = 1500;
        } else if (target.classList.contains('level-normal')) {
            return this.timeLevel = 1000;
        } else if (target.classList.contains('level-hard')) {
            return this.timeLevel = 500;
        }
    }

    _showing() {
        this.cellsIndex = this._random();
        this.emptyCells[this.cellsIndex].classList.add('show__cell');
    }

    _missed() {
        if (!this.emptyCells[this.cellsIndex].classList.contains('catching__cell')) {
            this.emptyCells[this.cellsIndex].classList.add('missed__cell');
        }
        this.emptyCells.splice(this.cellsIndex, 1);

        if (this.emptyCells.length === 0) {
            this.btnStart.textContent = 'End of the game';
            this.btnStart.disabled = 'true';
            return this._winner();
        }

    }
    _random() {
        return Math.floor((Math.random() * (this.emptyCells.length - 1)));
    }

    _winner() {
        const message = document.querySelector('.game-result');
        if (this.score.textContent <= (this.cellsCount - this.score.textContent)) {
            message.textContent = 'Game over. Try again...';
        } else {
            message.textContent = 'Congratulations! You are winner!';
        }
    }
}

const gameArea = document.querySelector('.table');
const btnStart = document.querySelector('.btn__start');
const btnLevels = document.querySelector('.btn__level');
const cellsList = gameArea.querySelectorAll('.table__cell');
let score = document.querySelector('.score');
const levelsList = document.querySelector('.levels-list');


btnLevels.addEventListener('click', () => {
    levelsList.style.display = 'block';
    btnLevels.style.display = 'none';
});

let timeLevel = levelsList.addEventListener('click', (event) => {
    levelsList.style.display = 'none';
    return player.level(event.target);
});

const player = new WhackAMole(btnStart, gameArea, cellsList, score, cellsList.length, timeLevel);

player.btnStart.addEventListener('click', (event) => {
    btnLevels.disabled = 'true';
    if (event.target.textContent === 'Start game') {
        player.start();
    } else {
        player.stop();
    }
});
player.gameArea.addEventListener('click', (event) => {
    if (event.target.classList.contains('catching__cell')) {
        return;
    }
    player.catch(event.target);
});


