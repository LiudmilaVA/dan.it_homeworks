import React, { Fragment } from 'react';
import { Products } from './pages/products';

function App() {
  return (
    <Fragment>
        <Products />
    </Fragment>
  );
}

export default App;
