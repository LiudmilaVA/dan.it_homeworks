import React from 'react';
import { Link } from 'react-router-dom';

import './style.scss';
export const Navigation = () => {

    return (
        <nav className='nav'>
            <h1 className='nav__title'>Навигация</h1>

            <Link to='/products' className='nav__item'>Товары</Link>
            <Link to='/favorites' className='nav__item'>Избранное</Link>
            <Link to='/basket' className='nav__item'>Корзина</Link>
        </nav>
    )
};

























