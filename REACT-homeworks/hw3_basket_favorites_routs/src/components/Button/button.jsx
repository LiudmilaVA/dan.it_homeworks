import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './button.css';

const Button = ({
      backgroundColor, text, onClick, className, ...attrs
    }) => {

    const onClickAction = e => {
          return onClick(e);
    };

    const classes = classNames(
        'btn',
        className,
    );

    return (
        <button
            className={classes}
            style={{backgroundColor: backgroundColor}}
            onClick={onClickAction}
            {...attrs}
        >
            {text}
        </button>
    );
};

Button.propTypes = {
    backgroundColor: PropTypes.string,
    text: PropTypes.string,
    onClick: PropTypes.func,
    className: PropTypes.string,
};

Button.defaultProps = {
    text: 'Default Button',
    onClick: () => {},
    className: '',
    backgroundColor: '#7CC6fE',
};

export default Button;
