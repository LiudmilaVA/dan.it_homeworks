import React, { useState, useEffect, useLayoutEffect } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import CardsList from '../CardsList/cardsList';
import Button from '../Button/button';

export const Basket = () => {
    const [inBasket, setInBasket] = useState(false);

    const getListLength = (length) => {
        (length > 0) ? setInBasket(true) : setInBasket(false)
    };

    return (
        <>
            <h2>Корзина</h2>

            {!inBasket &&
                <p>Ничего не добавлено в корзину. </p>
            }
            <CardsList
                forBasket
                isEmpty={getListLength}
            />

            {inBasket &&
                <Button text='Оформить заказ' className='btn__basket' />
            }
        </>
    );
};
