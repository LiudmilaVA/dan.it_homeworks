import React, { useState } from 'react';
import PropTypes from 'prop-types';

import Portal from '../Portal/portal';
import Button from '../Button/button';

import './modal.scss';

const Modal = ({
    onBasketChange, header, text, closeButton, btnOpenText, btnOpenColor, btnOpenClassName, actions
  }) => {

    const [isOpen, setIsOpen] = useState(false);

    const openModal = () => {
        setIsOpen(true);
    };

    const handleSubmit = () => {
        if (isOpen) {
            setIsOpen(false);
        } else {
            setIsOpen(true);
        }
        onBasketChange();
    };

    const handleCancel = () => {
        setIsOpen(false);
    };

    return (
        <>
            <Button
                onClick={openModal}
                text={btnOpenText}
                backgroundColor={btnOpenColor}
                className={btnOpenClassName}
            />

            { isOpen &&
            <Portal>
                <div className='modalOverlay'>
                    <div className='modalWindow'>

                        <div className='modalHeader'>
                            <div className='modalTitle'>{header}</div>
                            { closeButton &&
                            <span className='modalClose' onClick={handleCancel}>&#215;</span>
                            }
                        </div>

                        <div className='modalBody'>
                            {text}
                        </div>

                        <div className='modalFooter'>
                            <Button
                                text={actions[0].text}
                                onClick={handleSubmit}
                                style={{backgroundColor: actions[0].backgroundColor}}
                            />
                            <Button
                                text={actions[1].text}
                                onClick={handleCancel}
                                style={{backgroundColor: actions[1].backgroundColor}}
                            />
                        </div>
                    </div>
                </div>
            </Portal>
            }
        </>

    );
};


// class Modal extends React.Component {
//     state = {
//         isOpen: false,
//     };
//
//     openModal = () => {
//         this.setState({ isOpen: true });
//     };
//
//     handleSubmit = () => {
//         this.setState( function (prevState) {
//             return { isOpen: !prevState.isOpen
//             };
//         });
//         this.props.onBasketChange();
//     };
//
//     handleCancel = () => {
//         this.setState({ isOpen: false });
//     };
//
//     render() {
//         const { header, text, closeButton, btnOpenText, btnOpenColor, btnOpenClassName, actions } = this.props;
//         return (
//             <>
//                 <Button
//                     onClick={this.openModal}
//                     text={btnOpenText}
//                     backgroundColor={btnOpenColor}
//                     className={btnOpenClassName}
//                 />
//
//                 { this.state.isOpen &&
//                 <Portal>
//                     <div className='modalOverlay'>
//                         <div className='modalWindow'>
//
//                             <div className='modalHeader'>
//                                 <div className='modalTitle'>{header}</div>
//                                 { closeButton &&
//                                     <span className='modalClose' onClick={this.handleCancel}>&#215;</span>
//                                 }
//                             </div>
//
//                             <div className='modalBody'>
//                                 {text}
//                             </div>
//
//                             <div className='modalFooter'>
//                                 <Button
//                                     text={actions[0].text}
//                                     onClick={this.handleSubmit}
//                                     style={{backgroundColor: actions[0].backgroundColor}}
//                                 />
//                                 <Button
//                                     text={actions[1].text}
//                                     onClick={this.handleCancel}
//                                     style={{backgroundColor: actions[1].backgroundColor}}
//                                 />
//                             </div>
//                         </div>
//                     </div>
//                 </Portal>
//                 }
//             </>
//
//         )
//     };
// }

// const Modal = ({
//       header, isOpen, onCancel, onSubmit, text, closeButton, actions
//     }) => {
//
//     return (
//         <>
//             { isOpen &&
//             <Portal>
//                 <div className='modalOverlay'>
//                     <div className='modalWindow'>
//
//                         <div className='modalHeader'>
//                             <div className='modalTitle'>{header}</div>
//                             { closeButton && <span className='modalClose' onClick={onCancel}>&#215;</span> }
//                         </div>
//
//                         <div className='modalBody'>
//                             {text}
//                         </div>
//
//                         <div className='modalFooter'>
//                             <Button
//                                 text={actions[0].text}
//                                 onClick={onSubmit}
//                                 style={{backgroundColor: actions[0].backgroundColor}}
//                             />
//                             <Button
//                                 text={actions[1].text}
//                                 onClick={onCancel}
//                                 style={{backgroundColor: actions[1].backgroundColor}}
//                             />
//                         </div>
//                     </div>
//                 </div>
//             </Portal>
//             }
//         </>
//     );
// };
//
// class Sandbox extends Component {
//     state = { isOpen: false };
//
//     openModal = () => {
//         this.setState({ isOpen: true });
//     };
//
//     handleSubmit = () => {
//         this.setState( function (prevState) {
//             return { isOpen: !prevState.isOpen
//             };
//         });
//         console.log('Modal handleSubmit');
//     };
//
//     handleCancel = () => {
//         this.setState({ isOpen: false });
//     };
//
//     render() {
//         const { btnOpenText, btnOpenColor, header, text, actions, closeButton } = this.props;
//         return (
//             <Fragment>
//                 <Button
//                     onClick={this.openModal}
//                     text={btnOpenText}
//                     backgroundColor={btnOpenColor}
//                 />
//                 <Modal
//                     header={header}
//                     text={text}
//                     isOpen={this.state.isOpen}
//                     onCancel={this.handleCancel}
//                     onSubmit={this.handleSubmit}
//                     actions={actions}
//                     closeButton={closeButton}
//                 >
//                 </Modal>
//             </Fragment>
//         );
//     }
// }

Modal.propTypes = {
    header: PropTypes.string,
    isOpen: PropTypes.bool,
    onCancel: PropTypes.func,
    onSubmit: PropTypes.func,
    text: PropTypes.node,
    btnOpenText: PropTypes.string,
    btnOpenColor: PropTypes.string,
    actions: PropTypes.array,
    closeButton: PropTypes.bool,

};

Modal.defaultProps = {
    header: 'Modal title',
    isOpen: false,
    onCancel: () => {},
    onSubmit: () => {},
    text: null,
    btnOpenText: 'Modal open',
    btnOpenColor: 'black',
    closeButton: false,
    actions: [],
};


export default Modal;
