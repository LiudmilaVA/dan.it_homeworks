import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { FaShoppingBasket } from "react-icons/fa";

import './style.scss';

const BasketIcon = ({
    className, inBasket
    }) => {

    const classes = classNames(
        'basket',
        className
    );

    return (
        <>
            { inBasket &&
            <span>
               <FaShoppingBasket
                   className={classes}
               />
            </span>
            }
        </>
    );
};

BasketIcon.propTypes = {
    className: PropTypes.string,
    inBasket: PropTypes.bool,
};

BasketIcon.defaultProps = {
    className: '',
    inBasket: false,
};

export default BasketIcon;
