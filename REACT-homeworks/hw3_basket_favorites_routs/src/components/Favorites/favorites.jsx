import React, { useState, useEffect, useLayoutEffect } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import CardsList from '../CardsList/cardsList';

export const Favorites = () => {
    const [isFavorite, setIsFavorite] = useState(false);

    const getListLength = (length) => {
        (length > 0) ? setIsFavorite(true) : setIsFavorite(false)
    };

    return (
        <>
            <h2>Избранное</h2>
            { !isFavorite && <p>Ничего не добавлено в избранное. </p> }

            <CardsList
                forFavorites
                isEmpty={getListLength}
            />
        </>
    );
};
