import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Card from "../Card/card";
import classNames from 'classnames';

import "./cardsList.scss";

// const CardsList = ({
//     className, forFavorites, forBasket, forProducts, isEmpty
//   }) => {
//
//     const [products, setProducts] = useState([]);
//     const [favoritesList, setFavoritesList] = useState([]);
//     const [basketList, setBasketList] = useState([]);
//
//     const classes = classNames(
//         'cardsList',
//         className
//     );
//
//     useEffect(() => {
//        axios
//            .get('./productsDB.json')
//            .then(res => {
//                setProducts(res.data.products);
//            })
//            .catch(err => {
//                console.log(err);
//            });
//
//        getLocalStorage('favoritesList', setFavoritesList);
//        getLocalStorage('basketList', setBasketList);
//
//     }, []);
////
//     const getLocalStorage = (key, setFunction) => {
//         let nums = localStorage.getItem(key);
//         let arr = [];
//         if (nums) {
//             arr = JSON.parse(nums);
//             setFunction(arr);
//             return arr;
//         }
//         return arr;
//     };
//
//     const iconsHandler = (key, cardNumber, setFunction, keyName) => {
//         let arr = key;
//
//         if (isInLocalStorage(key, cardNumber)) {
//             const cardIndex = arr.indexOf(cardNumber);
//             arr.splice(cardIndex, 1);
//         } else {
//             arr.push(cardNumber);
//         }
//
//         setFunction(arr);
//         console.log(arr);
//         localStorage.setItem(keyName, JSON.stringify(arr));
//     };
//
//     const isInLocalStorage = (key, cardNumber) => {
//         if (key.includes(cardNumber)) {
//             return true;
//         } else {
//             return false;
//         }
//     };
//
//     const mappingCard = (card, isCloseButton= false, isBasketPage = false) => {
//         return <Card
//             key={card.article}
//             src={card.url}
//             title={card.name}
//             article={card.article}
//             price={card.price}
//             color={card.color}
//             isFavorite={ isInLocalStorage(favoritesList, card.article) }
//             inBasket={ isInLocalStorage(basketList, card.article) }
//             favoritesHandler={ () => iconsHandler(favoritesList, card.article, setFavoritesList, 'favoritesList')}
//             basketHandler={ () => iconsHandler(basketList, card.article, setBasketList, 'basketList')}
//             closeButton={isCloseButton}
//             isBasketPage={isBasketPage}
//         />
//     };
//
//     return (
//         <div className={classes} >
//             { forFavorites  && (
//                 <>
//                     <span onLoadedData={ isEmpty(favoritesList.length) } />
//
//                     { products.map((card, index) => {
//                         if (isInLocalStorage(favoritesList, card.article)) {
//                             return mappingCard(card);
//                         }
//                     })
//                     }
//                 </>
//             )}
//
//             { forBasket && (
//                 <>
//                     <span onLoadedData={ isEmpty(basketList.length) } />
//
//                     { products.map((card, index) => {
//                         if (isInLocalStorage(basketList, card.article)) {
//                             return mappingCard(card, true, true);
//                         }
//                     })
//                     }
//                 </>
//             )}
//             { forProducts && (
//                 <>
//                     { products.map((card, index) => {
//                         return mappingCard(card);
//                     })
//                     }
//                 </>
//             )}
//
//         </div>
//         );
// };

class CardsList extends React.Component {
    state = {
        products: [],
        favoritesList: [],
        basketList: [],
    };

    classes = classNames(
        'cardsList',
        this.props.className
    );

    fetchData() {
        fetch(`./productsDB.json`)
            .then(response => response.json())
            .then(data => this.setState({products: data.products }));
    }

    componentWillMount() {
        this.fetchData();

        this.getLocalStorage('favoritesList');
        this.getLocalStorage('basketList');
    }

    getLocalStorage(key) {
        let nums = localStorage.getItem(key);
        let arr = [];
        if (nums) {
            arr = JSON.parse(nums);
            this.setState({[key]: arr});
            return arr;
        }
        return arr;
    }

    iconsHandler = (key, cardNumber) => {
        let arr = this.state[key];

        if (this.getPresenceInLocalStorage(key, cardNumber)) {
            const cardIndex = arr.indexOf(cardNumber);
            arr.splice(cardIndex, 1);
        } else {
            arr.push(cardNumber);
        }

        this.setState({[key]: arr});
        localStorage.setItem(key, JSON.stringify(arr));
    };

    getPresenceInLocalStorage = (key, cardNumber) => {
        if(this.state[key].includes(cardNumber)) {
            return true;
        } else {
            return false;
        }
    };

    mappingCard(card, isCloseButton= false, isBasketPage = false) {
        return <Card
            key={card.article}
            src={card.url}
            title={card.name}
            article={card.article}
            price={card.price}
            color={card.color}
            isFavorite={this.getPresenceInLocalStorage('favoritesList', card.article)}
            inBasket={this.getPresenceInLocalStorage('basketList', card.article)}
            favoritesHandler={this.iconsHandler.bind(this, 'favoritesList', card.article)}
            basketHandler={this.iconsHandler.bind(this, 'basketList', card.article)}
            closeButton={isCloseButton}
            isBasketPage={isBasketPage}
        />
    }

    render() {
        const { forFavorites, forBasket, forProducts, isEmpty } = this.props;

        return (
        <div className={this.classes} >
            { forFavorites  && (
                <>
                    <span onLoadedData={ isEmpty(this.state.favoritesList.length) } />

                    { this.state.products.map((card, index) => {
                        if (this.getPresenceInLocalStorage('favoritesList', card.article)) {
                            return this.mappingCard(card);
                        }
                    })
                    }
                </>
            )}

            { forBasket && (
                <>
                    <span onLoadedData={ isEmpty(this.state.basketList.length) } />

                    { this.state.products.map((card, index) => {
                        if (this.getPresenceInLocalStorage('basketList', card.article)) {
                            return this.mappingCard(card, true, true);
                        }
                    })
                    }
                </>
            )}
            { forProducts && (
                <>
                    { this.state.products.map((card, index) => {
                        return this.mappingCard(card);
                    })
                    }
                </>
            )}

        </div>
        );
    }
}

export default CardsList;
