import React from 'react';

import { Layout } from '../views/Layout/layout';
import { Navigation } from '../views/Navigation/navigation';
import CardsList from '../components/CardsList/cardsList';

export const Products = () => {

    return (
        <Layout>
            <Navigation />
            <CardsList forProducts />
        </Layout>
    );
};
