import React from 'react';

import { Layout } from '../views/Layout/layout';
import { Navigation } from '../views/Navigation/navigation';
import { Favorites } from '../components/Favorites/favorites';

export const FavoritesPage = () => {

    return (
        <Layout>
            <Navigation />
            <Favorites />
        </Layout>
    );
};
