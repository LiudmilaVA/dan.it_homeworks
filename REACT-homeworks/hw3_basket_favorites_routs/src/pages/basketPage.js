import React from 'react';

import { Layout } from '../views/Layout/layout';
import { Navigation } from '../views/Navigation/navigation';
import { Basket } from '../components/Basket/basket';

export const  BasketPage = () => {

    return (
        <Layout>
            <Navigation />
            <Basket />
        </Layout>
    );
};
