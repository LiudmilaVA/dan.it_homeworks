import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { Products } from '../pages/products';
import { FavoritesPage } from '../pages/favoritesPage';
import { BasketPage } from '../pages/basketPage';

export const Routes = () => {
    return (
        <Switch>
            <Route path='/products' component={Products} />
            <Route path='/favorites' component={FavoritesPage} />
            <Route path='/basket' component={BasketPage} />
            <Redirect to='/products' />
        </Switch>
    );
};























// import React, { useState } from 'react';
// import { Switch, Route, Redirect } from 'react-router-dom';
//
// import { Mail } from '../pages/mail';
// import { Profile } from '../pages/profile';
// import { Registration } from '../pages/registration';
// import { Login } from '../pages/login.js';
//
// export const AuthContext = React.createContext();
//
// export const Routes = () => {
//     const [isAuthenticated, setIsAuthenticated] = useState(false);
//
//     return (
//         <AuthContext.Provider
//             value={{
//                 auth: {
//                     isAuthenticated,
//                     setIsAuthenticated,
//                 },
//             }}>
//             { isAuthenticated ? (
//                 <Switch>
//                     <Route path='/profile' component={Profile} />
//                     <Route path='/mail' component={Mail} />
//                     <Redirect to='/mail' />
//                 </Switch>
//             ) : (
//                 <Switch>
//                     <Route path='/login' component={Login} />
//                     <Route path='/registration' component={Registration} />
//                     <Redirect to='/login' />
//                 </Switch>
//             )
//             }
//         </AuthContext.Provider>
//     );
// };
