import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import Portal from '../portal/Portal';
import Button from '../button/Button';

import './Modal.css';

const Modal = ({
      header, isOpen, onCancel, onSubmit, text, closeButton, btnOpenText, actions
    }) => {

    return (
        <>
            { isOpen &&
            <Portal>
                <div className='modalOverlay'>
                    <div className='modalWindow'>

                        <div className='modalHeader'>
                            <div className='modalTitle'>{header}</div>
                            { closeButton && <span className='modalClose' onClick={onCancel}>&#215;</span> }
                        </div>

                        <div className='modalBody'>
                            {text}
                        </div>

                        <div className='modalFooter'>
                            {actions.map((button, index) => {
                                return <Button key={index+button.text} text={button.text} onClick={onCancel} style={{backgroundColor: button.backgroundColor}} />
                            }) }
                        </div>
                    </div>
                </div>
            </Portal>
            }
        </>
    );
};

class Sandbox extends Component {
    constructor(props) {
        super(props);
        this.state = { isOpen: false };
    }

    openModal = () => {
        this.setState({ isOpen: true });
    };

    handleSubmit = () => {
        this.setState( {isOpen: false });
    };

    handleCancel = () => {
        this.setState({ isOpen: false });
    };

    render() {
        return (
            <Fragment>
                <Button onClick={this.openModal} text={this.props.btnOpenText} />
                <Modal
                    header={this.props.header}
                    text={this.props.text}
                    isOpen={this.state.isOpen}
                    onCancel={this.handleCancel}
                    onSubmit={this.handleSubmit}
                    actions={this.props.actions}
                    closeButton={this.props.closeButton}
                >
                </Modal>
            </Fragment>
        );
    }
}

Modal.propTypes = {
    header: PropTypes.string,
    isOpen: PropTypes.bool,
    onCancel: PropTypes.func,
    onSubmit: PropTypes.func,
    text: PropTypes.node,
    btnOpenText: PropTypes.string,
    actions: PropTypes.array,
    closeButton: PropTypes.bool,

};

Modal.defaultProps = {
    header: 'modal title',
    isOpen: false,
    onCancel: () => {},
    onSubmit: () => {},
    text: null,
    btnOpenText: 'Modal open',
    closeButton: false,
    actions: [],
};


export default Sandbox;
