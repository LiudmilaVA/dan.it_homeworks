import React, { Fragment } from 'react';
import Button from "./components/button/Button";
import Modal from "./components/modal/Modal";


function App() {
  return (
    <Fragment>
      <Modal
          header='Do you want to delete this file?'
          text='Once you delete this file, it won’t be possible to undo this action.
Are you sure you want to delete it?'
          btnOpenText='Open first modal'
          actions = {[{text: 'Ok', backgroundColor: '#b3382c' }, {text: 'Cancel', backgroundColor: '#64342f'}]}
          closeButton={true}
      />

      <Modal
          header='Do you want to leave this page?'
          text='Click No, if you want to stay on this page.'
          btnOpenText='Open second modal'
          actions = {[{text: 'Ok', backgroundColor: '#2d30b4' }, {text: 'No', backgroundColor: '#181a65'}]}
      />
    </Fragment>
  );
}

export default App;
