import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { FaShoppingBasket } from "react-icons/fa";

import './style.scss';

const BasketIcon = ({
        className, inBasket, addInBasketHandler
    }) => {
    const classes = classNames(
        'basket',
        className
    );

    return (
        <>
            { inBasket &&
            <span>
               <FaShoppingBasket
                   className={classes}
                   onClick={addInBasketHandler}
               />
            </span>
            }
        </>
    );
};

BasketIcon.propTypes = {
    className: PropTypes.string,
    addInBasketHandler: PropTypes.func,
    inBasket: PropTypes.bool,
};

BasketIcon.defaultProps = {
    className: '',
    addInBasketHandler: () => {},
    inBasket: false,
};

export default BasketIcon;
