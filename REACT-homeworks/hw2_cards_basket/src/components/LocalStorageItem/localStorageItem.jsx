import React, { Component } from 'react';

export default class LocalStorageItem extends Component{

    constructor(props){
        super(props);
        this.state = {
            num: props.num,
            isSaved: false
        }
    }
    saveNum = () => { //нужно добавление дового ключа
        let nums = localStorage.getItem('myNums');
        let arr = [];
        if(nums) {
            arr = JSON.parse(nums);
            arr.push(this.state.num);
            arr = Array.from(new Set(arr));
        } else {
            arr.push(this.state.num);
        }
        localStorage.setItem('myNums', JSON.stringify(arr));
        this.setState({isSaved:true});
    };

    componentDidMount(){ //нужно получение все ключей
        let nums = localStorage.getItem('myNums');
        let arr = [];
        if(nums){
            arr = JSON.parse(nums);
            if( arr.includes(this.state.num) ){
                this.setState({isSaved: true});
            }
        }
    }

    render(){
        return (
            <p className={ this.state.isSaved ? "saved" : ""} >Hi. My number is { this.state.num }.
                <button onClick={this.saveNum}>Click to remember me</button></p>
        );
    }
}

// import React, { Component } from 'react';
//
// class LocalStorageItem extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             num: props.num,
//             isSaved: false
//         }
//     }
//     saveNum = (ev) => { //сохранить данное число при клике на кнопку
//         let nums = localStorage.getItem('muNums');
//         let arr = [];
//         if (nums) { //если за данным ключем массив есть
//             arr = JSON.parse(nums); //распарсить в массив
//             arr.push(this.state.num); //добавить в массив данное число, полученое из пропс
//             arr = Array.from(new Set(arr)); //удаляем дубликаты при попытке добавить то же число
//             localStorage.setItem('myNums', JSON.stringify(arr)); //устанавливаем в хранилище новое значение с новым числом
//         } else {
//             arr = [];
//             arr.push(this.state.num); //если за данным ключем нем значений, просто добавляем число
//             localStorage.setItem('myNums', JSON.stringify(arr));
//         }
//
//         this.setState({isSaved: true});
//     };
//
//     componentDidMount() { // ------------------------------------------------------------------------------------1
//         let nums = localStorage.getItem('myNums'); //все значения из сторедж
//         let arr = [];
//         if (nums) { // если там что-то есть
//             arr = JSON.parse(nums); //преврати это обратно в массив
//             if ( arr.includes(this.state.num)) { //если в списке есть это число
//                 this.setState({isSaved: true}); // то сделай его активным
//             }
//         }
//     }
//
//     render() {
//         return (
//             <p className={this.state.isSaved ? 'saved' : ''}>Hi. My number is { this.state.num }.
//                 <button onClick={this.saveNum}>Click to remember me</button>
//             </p>
//         )
//     }
//
// }
//
// export default LocalStorageItem;
