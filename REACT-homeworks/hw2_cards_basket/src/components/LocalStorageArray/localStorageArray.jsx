import React, { Component } from 'react';

import LocalStorageItem from '../LocalStorageItem/localStorageItem';

class LocalStorageArray extends Component {

    constructor(props){
        super(props);
        this.state = {
            items: [11,21,31]
        }
    }

    numSort = (a, b) => {
        if(a < b){
            return -1;
        }else if(b < a){
            return 1;
        }else{
            return 0;
        }
    }

    addNum = (ev) => {
        ev.preventDefault();
        let _items = this.state.items.sort(this.numSort);
        let max = Math.max( ..._items ) + 1;
        _items.push(max);
        this.setState({items: _items});
    };

    addFromLS = (num) => {
        let _items = this.state.items;
        _items.push(num);
        _items = Array.from(new Set(_items)).sort(this.numSort);
        this.setState({items: _items});
    };

    componentDidMount(){ //нужно
        let _items = localStorage.getItem('myNums');
        if(_items){
            _items = JSON.parse(_items);
            _items.forEach( this.addFromLS );
        }
    }

    render() {

        return (
            <div className="App">
                <p><button onClick={this.addNum}>Add an Item</button></p>

                {this.state.items && this.state.items.map( (item) => (
                    <LocalStorageItem key={item} num={item} /> //передать значение пропсом
                ))
                }
            </div>
        );
    }
}

export default LocalStorageArray;

// import React, { Component } from 'react';
// import LocalStorageItem from '../LocalStorageItem/localStorageItem';
//
// class LocalStorageArray extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             items: [11, 21, 31]
//         }
//     }
//     numSort = (a, b) => {
//         if (a < b) {
//             return -1;
//         } else if (b < a) {
//             return 1;
//         } else {
//             return 0;
//         }
//     };
//
//     addNum = (ev) => { // функция добавления нового элемента с новым числом - просто тут пример
//         ev.preventDefault();
//         let _items = this.state.items.sort(this.numSort); // сортируем числа от меньшего к большему
//         let max = Math.max(..._items); //ищем макисмальное с помощью деструктуризации
//         max += 1; // и уведичиваем на единицу, чтобы получить НОВОЕ число
//         _items.push(max); // добавляем его в отсортированные массив в конец
//         this.setState({items: _items}); //обновляем состояние заменив значение
//     };
//
//     addFromLS = (num) => {
//         let _items = this.state.items;
//         _items.push(num);
//         _items = Array.from(new Set(_items)).sort(this.numSort);
//         this.setState( {items: _items });
//     };
//
//     componentDidMount() {
//         let _items = localStorage.getItem('myNums'); //получить значение из хранилища
//         if (_items) { //если оно есть
//             _items = JSON.parse(_items); //распарсить его
//             _items.forEach( this.addFromLS ); //для каждого элем массива запустить функцию для сортировки и добавления нового элемента
//         }
//     }
//
//     render() {
//         return (
//           <div className='App'>
//               <p><button onClick={this.addNum}>Add an Item</button></p>
//
//               {this.state.items &&
//                   this.state.items.map((item) => (
//                       <LocalStorageItem key={item} num={item} />
//                   ))
//               }
//           </div>
//         );
//     }
// }
//
// export default LocalStorageArray;
