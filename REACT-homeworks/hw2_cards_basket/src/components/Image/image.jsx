import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import "./image.scss";

const Image = ({
                   src, alt, className, width, height, ...attrs
               }) => {

    const classes  = classNames(
        'image',
        className
    );

    return (
        <img
            src={src}
            alt={alt}
            className={classes}
            width={width}
            height={height}
            {...attrs}
        />
    );
};

Image.propTypes = {
    src: PropTypes.string,
    alt: PropTypes.string,
    className: PropTypes.string,
    width: PropTypes.number,
    height: PropTypes.number,
};

Image.defaultProps = {
    src: '',
    alt: 'image name',
    className: '',
    width: 200,
    height: 200,
};

export default Image;
