import React from 'react';
import ClassNames from 'classnames';
import Card from "../Card/card";

import "./cardsList.scss";

class CardsList extends React.Component {
    state = {
        products: [],
        favoritesList: [],
        basketList: [],
    };

    classes = ClassNames(
        'cardsList',
        this.props.className
    );

    fetchData() {
        fetch(`./productsDB.json`)
            .then(response => response.json())
            .then(data => this.setState({products: data.products }));
    }

    componentWillMount() {
        this.fetchData();

        this.getLocalStorage('favoritesList');
        this.getLocalStorage('basketList');
    }

    getLocalStorage(key) {
        let nums = localStorage.getItem(key);
        let arr = [];
        if (nums) {
            arr = JSON.parse(nums);
            this.setState({[key]: arr});
            return arr;
        }
        return arr;
    }

    iconsHandler = (key, cardNumber) => {
        let arr = this.state[key];

        if (this.getPresenceInLocalStorage(key, cardNumber)) {
            const cardIndex = arr.indexOf(cardNumber);
            arr.splice(cardIndex, 1);
        } else {
            arr.push(cardNumber);
        }

        this.setState({[key]: arr});
        localStorage.setItem(key, JSON.stringify(arr));
    };

    getPresenceInLocalStorage(key, cardNumber) {
        if(this.state[key].includes(cardNumber)) {
            return true;
        } else {
            return false;
        }
    }

    render() {
        return (
        <div className={this.classes}>
            { this.state.products.map((card, index) => {
                return <Card
                    key={card.article}
                    src={card.url}
                    title={card.name}
                    article={card.article}
                    price={card.price}
                    color={card.color}
                    isFavorite={this.getPresenceInLocalStorage('favoritesList', card.article)}
                    inBasket={this.getPresenceInLocalStorage('basketList', card.article)}
                    favoritesHandler={this.iconsHandler.bind(this, 'favoritesList', card.article)}
                    basketHandler={this.iconsHandler.bind(this, 'basketList', card.article)}
                />
            })
            }
        </div>
        );
    }
}

export default CardsList;

