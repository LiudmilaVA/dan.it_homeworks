import React  from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Image from "../Image/image";
import Modal from "../Modal/modal";
import StarIcon from "../StarIcon/starIcon";
import BasketIcon from '../Basket/basket';

import "./card.scss";

class Card extends React.Component {

     classes  = classNames(
        'card',
        this.props.className
    );

    render() {
        const { src, title, article, price, color, isFavorite, inBasket, favoritesHandler, basketHandler } = this.props;

        return (
            <>
                <div className={this.classes} style={{borderColor: color}} >
                    <Image src={src} alt={title} />
                    <h3 className='card__title'>{title}</h3>

                    <p>
                        <span className='card__article'>Артикул: {article}</span>
                        <span className='card__price'>Цена: {price}</span>
                    </p>
                    <StarIcon
                        favoritesHandler={favoritesHandler}
                        isFavorite={isFavorite}
                    />
                    <BasketIcon
                        addInBasketHandler={basketHandler}
                        inBasket={inBasket}
                    />
                    <Modal
                        header='Корзина'
                        text='Добавить в корзину?'
                        btnOpenText='Купить'
                        btnOpenColor='#015668'
                        actions = {[
                            {text: 'Добавить', backgroundColor: '#015668' },
                            {text: 'Отменить', backgroundColor: '#263f44'}
                            ]}
                        closeButton={true}
                        onBasketChange={basketHandler}
                    />
                </div>
            </>
        );
    }
}

Card.propTypes = {
    src: PropTypes.string,
    title: PropTypes.string,
    article: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
    ]),
    price: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
    ]),
    color: PropTypes.string,
};

Card.defaultProps = {
    src: 'Img unloaded',
    title: 'Title unable',
    article: '000',
    price: "000",
    color: 'green',
};

export default Card;
