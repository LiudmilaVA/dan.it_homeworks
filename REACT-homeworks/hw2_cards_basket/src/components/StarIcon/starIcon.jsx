import React  from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { FaRegStar } from 'react-icons/fa';
import { GiRoundStar } from 'react-icons/gi';

import './starIcon.scss';

const StarIcon = ({
        className, isFavorite, favoritesHandler
    }) => {

    const classes = classNames(
      'star',
       className
    );

    return (
      <>
          <span
              className={classes}
              onClick={favoritesHandler}>
              {isFavorite ? <GiRoundStar /> : <FaRegStar /> }
          </span>
      </>
    );
};

StarIcon.propTypes = {
    className: PropTypes.string,
    favoritesHandler: PropTypes.func,
    isFavorite: PropTypes.bool,
};

StarIcon.defaultProps = {
    className: '',
    favoritesHandler: () => {},
    isFavorite: false,
};

export default StarIcon;

// class Sandbox extends Component {
//
//     render() {
//         return (
//             <StarIcon
//                 className={this.props.classes}
//                 onClick={this.props.favoritesHandler}
//                 isAdded={this.state.isAdded}
//             />
//         );
//     }
// }

// const StarIcon = ({
//         className, onClick, isAdded,
//     }) => {
//
//     const onClickAction = e => {
//         return onClick(e);
//     };
//
//     const classes = classNames(
//       'star',
//        className
//     );
//
//     return (
//       <>
//           <span
//               className={classes}
//               onClick={onClickAction}>
//               {isAdded ? <GiRoundStar /> : <FaRegStar /> }
//           </span>
//       </>
//     );
// };
//
// class Sandbox extends Component {
//     state = {
//         isAdded: false,
//     };
//
//     addToFavorites = (event) => {
//         this.setState( function (prevState) {
//             return {
//                 isAdded: !prevState.isAdded,
//             }
//         });
//     };
//
//     render() {
//         return (
//             <StarIcon
//                 className={this.props.classes}
//                 onClick={this.addToFavorites}
//                 isAdded={this.state.isAdded}
//             />
//         );
//     }
// }

