import React, { Fragment } from 'react';
import CardsList from "./components/CardsList/cardsList";

function App() {
  return (
    <Fragment>
      <CardsList  />
    </Fragment>
  );
}

export default App;
